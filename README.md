Active Health, located in Park Ridge, IL offers holistic medicine integrative care chiropractic, acupuncture, weight loss & nutrition, rehabilitation. For relief from chronic pain and disease, infertility, obesity, type 2 diabetes, stress, sports injuries. Most insurance accepted, cash plans.


Address: 1550 N Northwest Hwy, Suite 206, Park Ridge, IL 60068, USA

Phone: 847-739-3120

Website: https://activehlth.com
